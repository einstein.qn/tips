
DOMAIN=$1
[ -z "$DOMAIN" ] && {
    echo "Missing parameter"
    echo
    echo "Usage: $0 [domain]"
    echo
    exit 1
}

rm -rf /etc/letsencrypt/live/$DOMAIN
rm -rf /etc/letsencrypt/renewal/$DOMAIN.conf
rm -rf /etc/letsencrypt/archive/$DOMAIN
rm -rf /etc/letsencrypt/archive/$DOMAIN-*
