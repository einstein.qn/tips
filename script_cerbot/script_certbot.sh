#!/usr/bin/env bash
#
# AUTHOR        : Nghia Nguyen
# EMAIL         : einstein.qn@gmail.com
#
# USAGE         : script_cerbot.sh -d <domain> -p <forward port>
# 

# ================ Parse options ==============================
function help(){
    echo "Usage: $0 -d <domain> -p <port>"
    echo
    echo "Options:";
    echo "-d <domain>    : Domain"
    echo "-p <port>      : Forward port"
    echo "-w             : Include sub-domain wwww"
    echo "-f <full url>  : Forward url. it will override port option"
    echo "-e [email]     : Notify Email"
    echo "-h             : Help"
    echo
}

NOTIFY_EMAIL=einstein.qn@gmail.com

while getopts hwp:d:e:f: flag
do
    case "${flag}" in
        p) FORWARD_PORT=${OPTARG};;
        f) FORWARD_URL=${OPTARG};;
        d) DOMAIN=${OPTARG};;
        e) NOTIFY_EMAIL=${OPTARG};;
        w) INCLUDE_WWW=true;;
        h) help; exit 0;;
        \?) #unrecognized option
            echo "Invalid option."; echo; help; exit 1;
        ;;
    esac
done

# check if domain or port is empty
[[ -z "$DOMAIN" ]] && { echo "Missing Parameters"; echo; help; exit 1; }

# =================================================================

FOLDER_CONF=/etc/nginx/sites-enabled
SNIPPET_FILE=/etc/nginx/snippets/letsencrypt.conf
LIST_DOMAIN=(
    $DOMAIN
)

[[ -z "$FORWARD_URL" ]] || PROXY_PASS=$FORWARD_URL
[[ -z "$FORWARD_PORT" ]] || PROXY_PASS=http://127.0.0.1:$FORWARD_PORT


# Check parameter
[ -z "$2" ] && {
    echo "Missing parameter"
    echo
    echo "Usage: $0 [domain] [forward port]";
    echo "Eg: $0 abc.com 8080";
    exit 1;
}

get_nginx_conf_before() {
    DOMAIN_NAME=$1;
    DOMAINS="$DOMAIN_NAME"
    [[ -z "$INCLUDE_WWW" ]] || DOMAINS="$DOMAINS www.$DOMAIN_NAME"

    cat << EOF
server {
    listen 80;
    server_name $DOMAINS;
    include /etc/nginx/snippets/letsencrypt.conf;
}
EOF
}

get_nginx_conf_final() {
    DOMAIN_NAME=$1;
    DOMAINS="$DOMAIN_NAME"
    [[ -z "$INCLUDE_WWW" ]] || DOMAINS="$DOMAINS www.$DOMAIN_NAME"

    cat << EOF
server {
    listen 80;
    server_name $DOMAINS;
    client_max_body_size 100M;
    include /etc/nginx/snippets/letsencrypt.conf;
    return 301 https://\$host\$request_uri;
}

server {

    listen 443 ssl;
    server_name $DOMAINS;

    include /etc/nginx/snippets/letsencrypt.conf;

    gzip on;
    gzip_vary on;
    gzip_min_length 1024;
    gzip_proxied expired no-cache no-store private auth;
    gzip_types text/plain text/css text/xml text/javascript application/x-javascript application/xml;
    gzip_disable "MSIE [1-6]\.";

    ssl_certificate           /etc/letsencrypt/live/$DOMAIN_NAME/fullchain.pem;
    ssl_certificate_key       /etc/letsencrypt/live/$DOMAIN_NAME/privkey.pem;


    ssl_session_cache  builtin:1000  shared:SSL:10m;
    ssl_protocols  TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers HIGH:!aNULL:!eNULL:!EXPORT:!CAMELLIA:!DES:!MD5:!PSK:!RC4;
    ssl_prefer_server_ciphers on;

    access_log            /var/log/nginx/$DOMAIN_NAME.access.log;
    error_log             /var/log/nginx/$DOMAIN_NAME.error.log;
    client_max_body_size 100M;

    location / {
        proxy_set_header        Host \$host;
        proxy_set_header        X-Real-IP \$remote_addr;
        proxy_set_header        X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header        X-Forwarded-Proto \$scheme;
        proxy_pass              $PROXY_PASS;
        proxy_read_timeout      120;
    }

  }
EOF
}

get_snippets_content() {
    cat << EOF
location ^~ /.well-known/acme-challenge/ {
  default_type "text/plain";
  root /var/www/certbot;
}
EOF
}

gen_snippets_if_not_exist() {
    [ -f $SNIPPET_FILE ] || { 
        echo "$SNIPPET_FILE is not exist. Create it."; 
        get_snippets_content > $SNIPPET_FILE
    }
}


gen_all_config_before() {
    for domain in "${LIST_DOMAIN[@]}"; do 
        echo "Gen config file for domain $domain"
        get_nginx_conf_before $domain > $FOLDER_CONF/$domain.conf
    done
    echo "======= DONE =============="
}

run_all_letsencrypt() {
    for domain in "${LIST_DOMAIN[@]}"; do 
        echo "======= Run letsencrypt for domain: $domain =============="
        CMD="docker run --rm -it -v /etc/letsencrypt:/etc/letsencrypt -v /var/www/certbot:/var/www/certbot  certbot/certbot \
            certonly --webroot -w /var/www/certbot \
            --email $NOTIFY_EMAIL \
            --agree-tos \
            --force-renewal \
            -d $domain"
        # include www.domain
        [[ -z "$INCLUDE_WWW" ]] || { 
            CMD="$CMD -d www.$domain"
        }
        eval $CMD
    done
    echo "======= DONE =============="
}

gen_all_config_final() {
    for domain in "${LIST_DOMAIN[@]}"; do 
        echo "Gen config file for domain $domain"
        get_nginx_conf_final $domain > $FOLDER_CONF/$domain.conf
    done
    echo "======= DONE =============="
}

gen_snippets_if_not_exist

gen_all_config_before
service nginx restart

run_all_letsencrypt

# Config nginx forward
[[ -z "$PROXY_PASS" ]] || { 
    gen_all_config_final
    service nginx restart
}
