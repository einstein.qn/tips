#!/usr/bin/env bash
#
# AUTHOR        : Nghia Nguyen
# EMAIL         : einstein.qn@gmail.com
#
# USAGE         : script_cerbot_renew.sh [-f] [-r] [-d <domain>]
#
# Description   : Use to renew cerbot domain
# 

# ================ Parse options ==============================
function help(){
    echo "Usage: $0 [-f] [-r] [-d <domain>]"
    echo
    echo "Options:";
    echo "-d <domain>    : Domain"
    echo "-f             : Force renewal"
    echo "-r             : Dry run"
    echo "-h             : Help"
    echo
}

while getopts frhd: flag
do
    case "${flag}" in
        f) FORCE=true;;
        r) DRY=true;;
        d) DOMAIN=${OPTARG};;
        h) help; exit 0;;
        \?) #unrecognized option
            echo "Invalid option."; echo; help; exit 1;
        ;;
    esac
done
# =============================================================

CMD="docker run --rm -v /etc/letsencrypt:/etc/letsencrypt -v /var/www/certbot:/var/www/certbot certbot/certbot renew"

[ -z "$DOMAIN" ] || { CMD="${CMD} --cert-name ${DOMAIN}"; }
[ ! $FORCE ] || { CMD="${CMD} --force-renewal"; }
[ ! $DRY ] || { CMD="${CMD} --dry-run"; }

echo "Run: $CMD"
eval $CMD;
echo "Reload nginx"
systemctl reload nginx
