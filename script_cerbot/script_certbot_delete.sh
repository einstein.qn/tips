#!/usr/bin/env bash
#
# AUTHOR        : Nghia Nguyen
# EMAIL         : einstein.qn@gmail.com
#
# USAGE         : script_cerbot_delete.sh [domain]
#
# Description   : Use to delete a cerbot domain
# 

DOMAIN=$1

if [ -z "$DOMAIN" ] 
then
    echo "Missing parameter"
    echo
    echo "Usage: $0 [domain]"
    echo
else
    docker run --rm -it -v /etc/letsencrypt:/etc/letsencrypt -v /var/www/certbot:/var/www/certbot  certbot/certbot delete --cert-name $DOMAIN
fi

